# На вход функция more_than_five(lst) получает список из целых чисел.
# Результатом работы функции должен стать новый список, в котором
# содержатся только те числа, которые больше 5 по модулю.

# Список из целых чисел
lst_1 = [1, 2, 3, -4, 5, 50, 6, 20, 7, 8, -9, 60, 10, -11, 12, 23]
lst_2 = [-1, 12, 65, 1, 18, 43, 96, 22]
lst_3 = [-9, 21, 2, -20, 4, -3, -2, 0]


def more_than_five(lst):
    new_lst = []
    for number in lst:
        if abs(number) > 5:
            new_lst.append(number)
    return new_lst


print("Результаты тестов")
print("Список 1: ", more_than_five(lst_1))
print("Список 2: ", more_than_five(lst_2))
print("Список 3: ", more_than_five(lst_3))
